class BasePageStructure(object):

    MARKET_BUTTON = "//*[@id='wd-_services']//a[text()='Маркет']"
    COMPUTERS_MENU_ITEM = '*//li[@data-department="Компьютеры"]'
    PADS_ITEM = "//*[@class='catalog-menu__list']//a[text()='Планшеты']"
    PRICE_MIN = "//*[@id='glf-pricefrom-var']"
    PRICE_MAX = "//*[@id='glf-priceto-var']"
    SHOW_MORE_BRAND = "//*[@class='button button_size_xs button_pseudo_yes button_theme_pseudo i-bem button_js_inited']"
    BRAND_ACER = "//*[@id='glf-7893318-267101']"
    APPLY_BUTTON = "//*[@class='n-filter-panel-aside__apply']/button"
    PRODUCTS = "//*[@class='n-snippet-card2__title']/a"
    SEARCH_FIELD = '//*[@id="header-search"]'
    SEARCH_BUTTON = "//*[@class='search2__button']"
    PRODUCTS_CARD = "//*[@class='n-title__text']"
